package com.kotlin

import java.io.Serializable

/**
 * Created by huaxia on 12/06/2018.
 */
interface State : Serializable

interface View {
    fun getCurrentState(): State
    fun restoreState(state: State)
}