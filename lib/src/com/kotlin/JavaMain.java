package com.kotlin;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * 15、内部类和嵌套类：默认嵌套类
 * Created by huaxia on 12/06/2018.
 */
public class JavaMain {
    public static void main(String[] args) {
        ObjectOutputStream out = null;
        try {
            out = new ObjectOutputStream(new FileOutputStream("result.obj"));
            out.writeObject(new ButtonI().getCurrentState());
//            out.writeObject(new ButtonKotlin().getCurrentState());
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
