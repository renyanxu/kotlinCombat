package com.kotlin;

import org.jetbrains.annotations.NotNull;

/**
 * Created by huaxia on 12/06/2018.
 */
public class ButtonI implements View {
    @NotNull
    @Override
    public State getCurrentState() {
        return new ButtonState();
    }

    @Override
    public void restoreState(@NotNull State state) {}

    public class ButtonState implements State {}
}
