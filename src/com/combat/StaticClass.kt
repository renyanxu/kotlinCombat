package com.example

/**
 * Created by renyanxu on 12/09/2017
 */

/**
 * 用object修饰的类是相当于静态类，Kotlin 中没有静态类的概念。
 * 这个叫 命名对象
 */
object StaticClass{
    const val TAG = "StaticClass"   //用const val 修饰常量

    fun doClick() {
        System.out.print("这是静态方法")
    }

    internal val Index = 1  //只在同一个模块(module)中才可见。
}