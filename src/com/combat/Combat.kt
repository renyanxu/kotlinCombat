package com.combat

import java.awt.Point
import java.beans.PropertyChangeEvent
import java.time.Duration
import java.time.LocalDate
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock
import kotlin.properties.Delegates


/** if表达式，类似（a>b）? a:b
 * */
fun max(a: Int, b: Int) = if (a > b) a else b

open class Person(val nick:String,val age:Int = 0){
    //2、自定义访问器
    var name:String = nick
        set(value) {
            println("field = $field")
            field = value
            println("field = $field")
        }
        get() = "[$field]"
}
//6、顶层函数和属性
fun addPerson( name: String, age: Int) {
    println("name = $name , age = $age")
}
//7、扩展函数和属性，Kotlin2类
fun String.lastChar(): Char = get(length - 1)

//12、kotlin接口的默认实现方法
interface KotlinI{
    fun add()
    fun minus(){
        println("kotlin的默认实现的方法不需要关键字")
    }
}
interface OtherI{
    fun minus(){
        println("kotlin的默认实现的方法不需要关键字")
    }
}
class JiekouI : KotlinI {
    override fun add() {}

    override fun minus() {
        super.minus()
        println("不符合要求需要重写")
    }
}

//13、接口中成员始终是open的，不能将其声明为final。抽象类中的抽象成员都是open,非抽象成员和普通类及成员都是final的。

//14
internal class ModelCanSee(val name: String)

//15 = lib
//16、密封类：定义受限的类继承结构,如下场景
sealed class Expr{
    class Num(val value:Int):Expr()
    class Sum(val left:String):Expr()
}
//class Summ:Expr(){}
fun eval(e: Expr) = when(e){
    is Expr.Num -> println(e.value)
    is Expr.Sum -> println(e.left)
}
//17、初始化类
class Man constructor(_name:String){
    val name:String
    init {
        name = _name
        println(name)
    }
}
//18、 数据类
data class Boy(var name: String, val age: Int)

//19、object
// a、单例
object Girl
//b、伴生对象
class A{
    companion object {
        val a = "a"
        fun b(){}
    }
}

class B{
    companion object Load{
        val name:String = "max"
    }
}

interface H {
    fun add()
}
class C{
    companion object :H{
        override fun add() {

        }
    }
}
fun load(h :H) {}

//半生对象的扩展函数
fun C.Companion.from(){}

//21、lambda
//demo中是java普遍的写法
fun findOldest(boys: List<Boy>) {
    var maxAge = 0
    var oldest: Boy? = null
    for (boy in boys) {
        if (boy.age > maxAge) {
            maxAge = boy.age
            oldest = boy
        }
    }
    println(oldest)
}

//class Book(val title: String, val authors: List<String>)
val runnable = Runnable { println("全局变量Runnable") }

lateinit var service: String

enum class OS { WINDOWS, LINUX, MAC, IOS, ANDROID }

fun main(args: Array<String>) {
    Person("Max").name = "ren"
//    3、is 等同instanceOf
    val p = Any()
    if (p is Person) {
        println("is person ${p.name}")
    }
//    4、javaClass方法等同于getClass
    String.javaClass

//    5、命名参数与默认参数
    addPerson("nick",age = 22)
//    addPerson(name = "nick",22)

//    8、vararg参数与展开运算符*
    val l = listOf(*args)

    //9、中缀调用
    val map = mapOf(1 to "one", 2 to 2)

    //10、解构声明
    val (number, name) = 1 to "one"
    println("number = $number , name = $name")
    for ((name, number) in map) {
        println("number = $number , name = $name")
    }

    //11、局部函数
    fun savePerson(p : Person){
        if (p.age > 20){
            //....
            //....
        }
    }
    savePerson(Person("min"))
    savePerson(Person("max",30))

//    18、equals
    var s1 = "max"
    val s2 = String().plus("max")
    if (s1 == s2) {
        println("对象相等")
    }
    if (s1 === s2) {
        println("引用相等")
    }

    val p1 = Person("max")
    val p2 = Person("max")
    if (p1 == p2) {
        println("Person对象比较 必须重写equals才会返回true")
    }

    //19、这里需要注意C类的名字被当做接口H的实例
    load(C)

    //c、匿名对象
    load(object : H {
        override fun add() {
        }
    })

//    21
    val boys = listOf(Boy("a", 19), Boy("b", 29))
    findOldest(boys)
//使用kotlin的库函数
    println(boys.maxBy { it.age })
    println(boys.maxBy(Boy::age))//成员引用替换法

//    22、语法
    val sum = { x: Int, y: Int -> x + y }
    println("和等于 ${sum(1, 2)}")

//    23
//    fun btClicks(button :Button){
//        var clicks = 0
//        button.onClick{ clicks++ }
//        return clicks
//    }

//    24、成员引用
    fun getString() = "helloworld"
    run(::getString)

//    25、集合的函数式API
//    a、 filter map
    val list  = listOf(1,2,3,4)
    println(list.filter { it % 2 == 0 })

    val list2 = listOf(1,2,3,4)
    println(list2.map { it * it })
    val listBoy = listOf(Boy("max", 20), Boy("min", age = 30), Boy(name = "helloworld", age = 19), Boy("xiao",19))
    println(listBoy.map { it.name })//只需要名字的集合

    //想要一个大于25岁的名字集合,多个调用链接起来
    val newList = listBoy.filter { it.age > 25 }.map { it.name }
    println("new list = $newList")

    //b、all any count find
    val dayu25 = {b:Boy -> b.age > 25}
    println("是否所有元素都满足判断式 = ${listBoy.all(dayu25)}")
    println("是否至少存在一个匹配的元素 = ${listBoy.any(dayu25)}")
    println("有多少元素满足 = ${listBoy.count(dayu25)}")
    println("找到第一个满足判断式的元素 = ${listBoy.find(dayu25)}")

//    c、groupBy
    println("根据判断式来分组 = ${listBoy.groupBy(Boy::age)}")

//    d、flatMap \ flatten
    val strings = listOf("abc","def")
    println(strings.flatMap { it.toList() })

    val listofList = listOf(listOf("123"), strings)
    println(listofList.flatten())

//    26、 a.序列
    listBoy.asSequence().filter(dayu25).map { it.name }.toList()

//    b.惰性
//    demo1
    println("start")
    list.asSequence().map {
        println("map ($it)")
        it * it
    }.filter {
        println("filter($it)")
        it % 2 == 0
    }
    println("end")

//    demo2
    println(list.asSequence().map {
        println("map = $it")
        it * it
    }.find {
        println("find = $it")
        it > 3
    })

//    顺序影响性能 demo3
    listBoy.asSequence().map(Boy::name).filter { it.length > 4 }.toList()

    listBoy.asSequence().filter { it.name.length > 4 }.map(Boy::name).toList()//性能更高效

//    c、创建序列
    val numberS = generateSequence(0, { it + 1 })
    val numberTo100 = numberS.takeWhile { it <= 100 }
    println(numberTo100.sum())

//26（2）、函数式接口
    Thread {
        s1 = ""
        println("SAM接口")
    }.start()

    Thread {
        println("SAM接口")
    }.start()
//    上下等同
    Thread(runnable).start()

    //或者这样，对象表达式每次调用都会创建一个新的实例
    Thread(object :Runnable{
        override fun run() {
            println("对象表达式 Runnable")
        }
    }).start()

    //    27、with,apply
//    java传统写法
    fun alphabet(): String {
        val result = StringBuilder()
        for (letter in 'A'..'Z')
            result.append(letter)
        result.append("\nall the alphabet - normal")
        return result.toString()
    }
    println(alphabet())
//    使用with
    fun alphabetWith(): String {
        return with(StringBuilder()) {
            for (letter in 'A'..'Z')
                this.append(letter)
            append("\nall the alphabet - with")
            toString()
        }
    }
    println(alphabetWith())
//    apply
    fun alphabetApply() = StringBuilder().apply {
        for (letter in 'A'..'Z')
            this.append(letter)
        append("\nall the alphabet - apply")
    }.toString()
    println(alphabetApply())

//    标准库函数buildString()更简化
    fun alphabetBuildString() = buildString {
        for (letter in 'A'..'Z')
            this.append(letter)
        append("\nall the alphabet - buildString")
    }

    //    28、kotlin空安全
    fun getNameLength(boy: Boy?): Int {
        return boy?.name?.length ?: 0
    }
//  32、let函数
    var letV1 :String? = "a"
    var letV = letV1?.let {
        it+"111"
        println("let: $it = ${it.length}")
        it
    }
    println("let: $letV")

    var letV2 :String? = "b"
    with(letV2) {
//        this +"b"
        plus("222")
        println("with: $this = ${this?.length}")
    }
//    var letV3 :String? = "c"
    var letV3 = "c".apply {
         this + "333"
//        plus("c")
//        println("apply: $this = ${this.length}")
    }
    println("apply: $letV3")

//    33、lateinit
    service = "lateinit fun"
//  34、可控类型的扩展
    if (letV1.isNullOrEmpty()) {
        println("letv is null")
    }

    //    35、类型参数的可空性
    fun <T> printFun(t: T) {
        println(t?.toString())//t可能为空，所以必须用安全调用
    }
    printFun(null)

// 37、Nothing
    fun fail():Nothing{
        throw  NullPointerException()
    }

// 40、重载算术运算符
    data class Point(val x: Int, val y: Int) {
        operator fun plus(other: Point): Point {
            return Point(x + other.x, y + other.y)
        }

        override fun equals(other: Any?): Boolean {
            if (other === this) return true
            if (other !is Point) return false
            return other.x == x && other.y == y
        }

    }

    val point1 = Point(1,1)
    val point2 = Point(2,2)
    println(point1 + point2)    //通过“+”号来调用plus方法

    operator fun Point.plus(other: Point): Point {  //也可以定义为扩展函数
        return Point(x + other.x, y + other.y)
    }

    //位运算符
    var a = 1 and 2 //按位与

    //b
    println("等号运算符 == ：${Point(10,20) == Point(10,20)}")

    //42。处理日期的区间
    val now = LocalDate.now()
    val vacation = now .. now.plusDays(10)
    println(now.plusWeeks(1) in vacation)   //检测一个特定的日期是否属于这个区间

    val num = 9
    println("0..num = ${0..num + 1}")//打印 0..10

    (0..5).forEach { item ->
//        println("rangeTo it = $it")
        println("rangeTo it = $item")
    }

    //43
    val (age,Name) = Pair(1,"name")

    //将一个文件名分割成名字和扩展名
    data class NameComponents(val name: String, val extension: String)

    fun splitFilename(fullName: String): NameComponents {
        val result = fullName.split('.', limit = 2)
        return NameComponents(result[0], result[1])
    }
    //使用解构声明的方式，可以很轻松的展开结果
    val (firstName, ext) = splitFilename("example.kt")
    println(firstName)
    println(ext)

    //数组和集合上也有定义componentN函数，可以使用解构声明
    fun splitFilename2(fullName: String): NameComponents {
        val (name, ext) = fullName.split('.', limit = 2)
        return NameComponents(name, ext)
    }

//    46、函数类型
    val summ = { xx: Int, yy: Int -> xx + yy }  //summ和action变量被自动推倒为函数类型
    val action = { println(123) }
    //显示类型声明
    val summ2: (Int, Int) -> Int = { xx, yy -> xx + yy }
    val action2: () -> Unit = { println(123) }

    //简单版的String扩展方法，参数为函数类型
    fun String.filter(predicate: (Char) -> Boolean): String {
        val sb = StringBuilder()
        for (index in 0 until length) {
            val element = get(index)
            if (predicate(element)) sb.append(element)
        }
        return sb.toString()
    }

    println("ab2c".filter {  it in 'a'..'z'}) // 输出：abc


//    47、返回函数的函数
    val shun_feng = "shun_feng"
    val yuan_tong = "yuan_tong"
    class Order(val intemCount:Int)

    fun getShippingCostCalculator(express: String): (Order) -> Double {
        if (shun_feng == express) {
            return { order -> 12 + 10.0 * order.intemCount } //返回lambda
        }
        return { order -> 10 + 8.0 * order.intemCount }
    }

    val calculator = getShippingCostCalculator(shun_feng)
    println(" express cost ${calculator(Order(3))}")

//  48、通过lambda可去重复代码
    data class SiteVisit(val path: String, val duration: Double, val os: OS)//保存网站每次访问的路径、持续时间和用户的操作系统的类

    val log = listOf(SiteVisit("/",34.0,OS.WINDOWS),
            SiteVisit("/",22.0,OS.MAC),
            SiteVisit("/login",12.0,OS.WINDOWS),
            SiteVisit("/signup",8.0,OS.IOS),
            SiteVisit("/",16.3,OS.ANDROID))

    //需求计算windows机器的平均访问时间
    val windowsDuration = log
            .filter { it.os == OS.WINDOWS }
            .map(SiteVisit::duration)
            .average()
    println(windowsDuration)

//  增加相同需求，计算其它平台平均访问时间，比如Mac,java思想通常是抽取出一个公用的方法
    //抽取扩展函数
    fun List<SiteVisit>.averageDurationFor(os: OS) = filter { it.os == os }.map(SiteVisit::duration).average()
    println(log.averageDurationFor(OS.MAC))
    println(log.averageDurationFor(OS.ANDROID))

//    但是需求变化多端，可能需要同时计算多个平台，比如移动端：Android 和IOS；或者更加复杂的条件需求
//    可以这样
    println(log.averageDurationFor(OS.ANDROID) + log.averageDurationFor(OS.IOS))
//    也可以这样
    val mobileDuration = log
            .filter { it.os in setOf(OS.IOS,OS.ANDROID) }
            .map(SiteVisit::duration)
            .average()
    println(mobileDuration)

//    使用高阶函数去除重复代码
    fun List<SiteVisit>.averageDurationFor(predicate: (SiteVisit) -> Boolean) =
            filter(predicate).map(SiteVisit::duration).average()

    println(log.averageDurationFor { it.os in setOf(OS.ANDROID,OS.IOS) })
    println(log.averageDurationFor { it.os == OS.IOS && it.path == "/signup"})

//    49、内联函数

//    synchronized() 自定义

    fun <T> synchronizeds(lock: Lock, block: () -> T): T {
        lock.lock()
        try {
            return block()
        } finally {
            lock.unlock()
        }
    }


    println("----自定义-----")


    val lock = ReentrantLock()
    synchronizeds(lock){
        println("lock1")
    }

    synchronizeds(lock){
        println("lock12")
    }

    synchronizeds2(lock){
        println("lock123")
    }

    println("----kotlin自带-----")

    synchronized(lock){
        println("lock2")
    }

}

inline fun <T> synchronizeds2(lock: Lock, block: () -> T): T {
    lock.lock()
    try {
        return block()
    } finally {
        lock.unlock()
    }
}
