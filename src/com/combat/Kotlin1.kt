package com.example.combat

/**
 * Desc :
 * Created by renyanxu
 */

/** if表达式，类似（a>b）? a:b
 * */
fun max(a: Int, b: Int) = if (a > b) a else b

open class Person(var nick:String){
    var name:String = nick
        set(value) {
            println("field = $field")
            field = value
            println("field = $field")
        }
        get() = "[$field]"
}


fun main(args: Array<String>) {
    Person("Max").name = "ren"
    Person("Max").name = "ren"
    val p = Any()
    if (p is Person) {
        println("is person ${p.name}")
    }
}