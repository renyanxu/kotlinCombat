package com.example

import org.apache.commons.lang3.RandomUtils

/**方法基本定义
 *
 * 基本类型：Byte(8)、Short(16)、Int(32)、Long(64)、Float(32)、Double(64)
 * 语句后面的分号可以省略，如果一行两条语句，那语句中间需要用分号隔开。
 * koltin编译生成xxxKt.class文件，只是文件名不同，但都是.class文件。
 * */
class MainKotlin {

    companion object {  //伴随对象，类似java静态成员
        val a = "1.0"
        fun doclick(){

        }
    }

    fun sum(a: Int, b: Int): Int {
        return a + b
    }

    //只有一个表达式作为方法体，以及自推导型的返回值
    fun minus(a: Int, b: Int) = a - b

    fun sum(a: Int): Unit {
        //返回空
//    字符串模板
        println("a = $a")
    }

    fun minus(a: Int) {
        //返回的空类型可以省略
        val s1 = "a is b"
        var s2 = "${s1.replace("is", "was")}, but no is $a"
        println("a - 1 = ${a - 1}, $s2")
    }

    /**变量
     * 具有可变 var 与不可变 val 之分
     * 具有空与不空之分
     * */
    fun localVariable() {
//  局部变量
        val a: Int = 1
        val b = 2   //类型自推导
        val c: Int  //var\val 没有初始化时要指定类型。如果是成员变量必须初始化。
        val d = 100_000 //下划线只是增加可读性
        var e = "demo"
        var g: String
        var h: String?  //类型后面的问号表示此变量可为null
        println(d)
    }

    //    成员变量一般情况必须初始化。但也可用 lateinit 修饰延迟赋值,不能修饰基本类型。
    val f = 1
    val z: Int? = null
    var w: String? = ""     //类型后面的问号表示此变量可为null
    lateinit var kk: Any    //Any 类型等同java中的Object

    /**数组*/
    val arr: Array<Int> = arrayOf(10, 25, 38) //参数是元素   //Array(5, { i -> i + 1 })
    val arr2: Array<String?> = arrayOfNulls(5)   //元素可能为空的数组
    val arr3 = emptyArray<Int>()    //空数组
    val arrInt: IntArray = intArrayOf(1, 2, 3) //Array<Int> 与 IntArray 两种类型，还有ByteArray,ShortArray等

    fun traverse(arr: Array<Int>) {
        //遍历数组
        for (i in arr) {
            println("元素 = $i")
        }

        for (i in arr.indices) {
            println("index = $i")
        }
        arr.forEach {
            println(" = $it")
        }
    }

    /**集合*/
    val list = listOf("a", "b", "c")
    val list2: List<Int> = listOf(1, 2)
    val list3 = mutableListOf(1.0, 2.0)
    //创建list、set、map 在kotlin中的标准库方法：listOf()、mutableListOf()、setOf()、mutableSetOf()、mapOf()、mutableMapOf()
    //map 遍历 for((key,value）in map）{ ... }


    /**
     * if
     * 在kotlin中没有三元运算符，if可做表达式必须有else，返回值必须赋值给一个变量
     *
     * */
    val max = if (f > arr[1]) f else z
    //
    val mins = if (f > arr[0]) {
        println(">")
        list.filter {
            it.length != 1
        }
        f
    } else {
        println("<")
        arr[0]
    }

    /**
     * when 等同于switch
     * 支持所有类型
     */

    fun whenFun() {
        val x = 1
        when(x){
            1 -> println("1")
            2,3 ->{
                println("2,3")
            }
            sum(1,2) -> println("3")    //用表达式作为分支条件
            else -> println("") //等同于default
        }
        val a = ""
        when (a) {
            "b" -> print("")
        }
        val b = 3.0
        when (b) {
            1.0 -> print("")
        }
        val c = true
        when (c) {
            true -> print("")
        }
    }


}

/***
 * 1-200之间获取100个随机数，然后取平均值的2/3为结果
 */

fun compute(sum: Int) {
    var ping = 0
    for (ii in 0..10) {
        var num: Int = 0
        for (i in 1..sum) {
            val random = RandomUtils.nextInt(1, 76)
//        println("随机数：$random，index: $i")
            num += random
        }
//    println("总和：$num")
        num = num / sum * 2 / 3
        ping += num
        println("人数：$sum , 随机数计算结果 = $num")
    }
    println("10次的平均结果 = ${ping/11}")
}

fun time(time:Int){
    for(i in (147-time)..147){
        compute(i)
    }
}

fun min() {
    val c = try {
        12
    } catch (e: Exception) {
        print("exception")
        10
    }
    println("c = $c")
}


fun main(args: Array<String>) {
    println("start")
    val kotlin = MainKotlin()   //初始化对象，没有new关键字
    kotlin.kk = 1

    println(kotlin.sum(1, 2))   //方法可当参数传入
    kotlin.minus(3)
    kotlin.localVariable()
    kotlin.traverse(kotlin.arr)
    val demo = KotlinDemo("")
    demo.let {
        demo.name
    }
//    public inline fun <T, R> T.let(block: (T) -> R): R {
//        return block(this)
//    }
    demo!!.name = " hell "
//    time(147)
    compute(100)
    StaticClass.doClick()
    MainKotlin.doclick()
    min()
}

