package com.combat;

import java.util.ArrayList;
import java.util.Optional;

/**
 * Created by huaxia on 12/06/2018
 */
public class JavaLei {
    public static void main(String[] args) {
        CombatKt.addPerson("nick", 22);
        StringFun.getFirst("max");
        Boy boy = new Boy(null,20);
        getNameLenght(boy);
        A.Companion.b();
        A.Companion.getA();
        B.Load.getName();
    }

    interface JavaI {
        default void add() {
            System.out.println("java8 的默认实现方法需要加 default 关键字");
//           在 java 8 之前,接口与其实现类之间的 耦合度 太高了（tightly coupled），当需要为一个接口添加方法时，
//           所有的实现类都必须随之修改。默认方法解决了这个问题，它可以为接口添加新的方法，而不会破坏已有的接口的实现。
        }
    }

//普遍java判断null写法
    static int getNameLenght(Boy boy) {
        if (boy != null) {
            String name = boy.getName();
            if (name != null) {
                return name.length();
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }
//使用java8 的Optional
    static int getLenght(Boy boy) {
        return Optional.ofNullable(boy).map(b -> b.getName()).map(n -> n.length()).orElse(0);
    }

}
