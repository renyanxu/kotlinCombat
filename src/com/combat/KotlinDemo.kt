package com.example

/**
 * 用 open 修饰的类允许被继承，在kotlin中类默认不允许继承，
 * 同样只有open的方法子类才能重写
 */
open class KotlinDemo(var name: String?) {
    //如果非抽象类没有定义构造函数，编译器会自动添加无参的public构造函数。

    init {  //主构造器的逻辑写在init block里，可以直接使用主构造器的参数。
        name = ""

        var niming = object : MainClass() { //匿名内部类

            override fun doClick() {
            }

        }
    }

    //第二构造器:需要用constructor来修饰，并且在有主构造器的时候，一定要使用this直接或间接的调用主构造器。
    constructor(age: Int, name2: String) : this(name2) {
        println(age)
    }


    open fun doClick() {
//        add(::test)//推荐下面的调用方式。
        add({test()})
    }


    lateinit var a: String   //lateinit
    var b: Int? = null

//    @CallSuper
    public fun test(): Int {
        val b: Int
        a = ""
        return 1
    }
    fun num(a:Int):Int{
        return 1
    }

    fun add(a: () -> Int):String {
        return "Str+${a}"
    }

    class Qiantao {     //类可以嵌套在其它类中
        val cc = 1
    }

    inner class NeibuLei{   //用 inner 标记可以访问外部类成员，内部类拥有外部类的一个对象引用。
        val str = name
    }

}
