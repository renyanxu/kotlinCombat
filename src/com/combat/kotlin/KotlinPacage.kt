package com.combat.kotlin

import com.combat.ModelCanSee
import com.combat.addPerson
import com.combat.lastChar

/**
 * Created by huaxia on 12/06/2018
 */
fun main(args: Array<String>) {
    addPerson("max",22)
    "name".lastChar()
    ModelCanSee("max").name
}